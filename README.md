
evgevrdata
======
European Spallation Source ERIC Site-specific EPICS module : evgevrdata

Info about how to group PVA data
https://github.com/epics-base/pva2pva
https://epics-base.github.io/pva2pva/qsrv_page.html#qsrv_group_def
https://gitlab.esss.lu.se/jimlarsson/mytestioc/-/blob/master/e3-ex6/ex6/ex6App/Db/circle.db

Additonal information:

* [Documentation](https://confluence.esss.lu.se/display/IS/Integration+by+ICS)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/evgevrdata-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
