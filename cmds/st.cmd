# This should be a test startup script
require evgevrdata

epicsEnvSet("P", "test:")

iocshLoad("$(evgevrdata_DIR)/evgevrdata.iocsh", "P=$(P), R=")

iocInit()

dbpf test:val0 2
dbpf test:val1 3
#EOF
