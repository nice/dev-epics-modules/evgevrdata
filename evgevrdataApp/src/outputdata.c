#include <stdio.h>
#include <stdlib.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <dbDefs.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <link.h>
#include <string.h>
#include <sys/time.h>



static int outputdata(aSubRecord *precord) {
 
    /* INPA and OUTA fields are accessed by precord->a and precord->vala*/
    /* epicsInt64 or epicsUInt64 */
    epicsTimeStamp tick;
    unsigned long arr_len;
    arr_len = precord->nova;
    int counter,  out_events[arr_len];

    if(!dbGetTimeStamp(&(precord->inpa),&tick)){
        /* Constant to convert from EPICS to Unix epoch (20*365.25*24*3600 = 631152000) */
        // input counter value
        counter = (*(int *)precord->a);
        //put values in array
        out_events[0] = counter;
        out_events[1] = tick.secPastEpoch + 631152000;
        out_events[2] = tick.nsec;
        // configure array size
        precord->neva = arr_len;
        // copy data to output array
        //memcpy(precord->vala, out_events, arr_len * sizeof(out_events[0]));
        memcpy(precord->vala, out_events, arr_len * sizeof(out_events[0]));
        *(int *)precord->valb = out_events[0];
        *(int *)precord->valc = out_events[1];
        *(int *)precord->vald = out_events[2];

        return 0;
    } else {
        printf("Could not retrieve timestamp \n");
        return -1;
    }

}
 
/* Note the function must be registered at the end!*/
epicsRegisterFunction(outputdata);
